package main

import (
	"context"
	"highload-service/handlers"
	"highload-service/kafka"
	"log"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"github.com/redis/go-redis/v9"
)

var (
	sysExch kafka.Topic
	userMsg kafka.Topic
)

func main() {
	// Start Kafka
	sysExch.Fill(os.Getenv("SYS_EXCH"))
	userMsg.Fill(os.Getenv("USER_MSG"))
	topics := kafka.Topics{
		sysExch,
		userMsg,
	}
	kafka.Start(os.Getenv("AK_ADDR"), topics)
	go handlers.Kafka()

	// Redis
	client := redis.NewClient(&redis.Options{
		Addr: "redis-service:6379",
	})
	_, err := client.Ping(context.Background()).Result()
	if err != nil {
		log.Fatalf("Redis connection failed: %v", err)
	}
	handlers.ConnRedis = client

	// Handler
	handlers.Highload()
}
