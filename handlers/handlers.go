package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"highload-service/kafka"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/IBM/sarama"
	_ "github.com/joho/godotenv/autoload"
	"github.com/redis/go-redis/v9"
)

var (
	ConnRedis   *redis.Client
	usrProducer sarama.AsyncProducer
	highload    kafka.Topic
	notices     kafka.Topic
	requestsMsg = make(chan []byte)
	result      Calc
)

type Calc struct {
	Date    time.Time
	Message string
}

func Kafka() {
	usrProducer = kafka.NewProd()
	highload.Fill(os.Getenv("HIGHLOAD"))
	notices.Fill(os.Getenv("NOTICES"))
	highload.Consume(requestsMsg)
}

func Highload() {
	log.Println("Start highload service...")
	for {
		<-requestsMsg
		answer := getData()
		notices.Produce([]byte(answer), usrProducer)
	}
}

func getData() string {
	now := time.Now()
	time.Sleep(1 * time.Second)
	result = Calc{now, fmt.Sprintf("Highload: %v", rand.Int())}
	// Redis transaction loop
	i := 1
	for {
		// Start optimistic locking
		err := ConnRedis.Watch(context.Background(), func(tx *redis.Tx) error {
			oldResults, err := tx.Get(context.Background(), "results").Result()
			if err != nil && err != redis.Nil {
				log.Printf("Failed to get old record: %v", err)
			}
			var results []Calc
			if oldResults != "" {
				err := json.Unmarshal([]byte(oldResults), &results)
				if err != nil {
					log.Printf("JSON deserializing failed: %v", err)
				}
			}
			results = append(results, result)
			log.Println(results)
			jsonData, err := json.Marshal(results)
			if err != nil {
				log.Printf("Serializing to JSON failed: %v", err)
				return err
			}
			_, err = tx.TxPipelined(context.Background(), func(pipe redis.Pipeliner) error {
				expiration := 60 * time.Minute
				pipe.Set(context.Background(), "results", jsonData, expiration)
				return nil
			})
			switch {
			case err == redis.TxFailedErr:
				return err
			case err != nil:
				log.Printf("Unexpected highload loop error: %v", err)
				return err
			default:
				log.Printf("Updated 'results' in Redis cache in %v loop", i)
				return nil
			}
		}, "results")
		if err != nil {
			i++
			continue
		}
		break // Break the loop if transaction was successful
	}
	return result.Message
}
