FROM alpine:latest

COPY ./main /app/main

RUN chmod +x /app/main

RUN <<EOF cat >> /app/.env
# Kafka address
AK_ADDR="kafka-service:9092" # "localhost:9092,localhost:9093"

# Topics (name,partitions,replication)
SYS_EXCH="sys-exch,3,1"
USER_MSG="user-msg,1,1"

# Destination (name,partition)
HIGHLOAD="sys-exch,0,1"
NOTICES="user-msg,0,1"
EOF

WORKDIR /app

CMD ["./main"]
