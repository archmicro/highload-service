# Highload microservice
The application simulates a 5 second delay in data processing when
receiving a message from the Apache Kafka "sys-exch" topic. The
response contains a string with a random number. It is sent to the
"user-msg" topic and saved in Redis for 1 hour.